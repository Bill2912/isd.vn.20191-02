drop database `db_itss`;

CREATE DATABASE  IF NOT EXISTS `db_itss`;
CREATE TABLE `db_itss`.`station` (
  `id` int(16) NOT NULL,
  `name` varchar(45) not NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `db_itss`.`location` (
  `ID_Station1` int(16)  NOT NULL,
  `ID_Station2` int(16)  Not NULL,
  `distance`  double(10,2) not null,
  foreign key (`ID_Station1`) references station(id),
  foreign key (`ID_Station2`) references station(id)
);
CREATE TABLE `db_itss`.`target` (
  `ID_Ticket` varchar(16) NOT NULL,
  `ID_Code` varchar(16) NOT NULL,
  PRIMARY KEY (`ID_Ticket`)
);
CREATE TABLE `db_itss`.`24hticket` (
  `ID_PseudoBar` varchar(8) NOT NULL,
  `ID_Target` varchar(16) NOT NULL,
  `ID_Code`  varchar(16),
  `active_time` datetime ,
  `ID_Root` int(10),
  PRIMARY KEY (`ID_Target`),
  foreign key (`ID_Target`) references target(ID_Ticket)
);
CREATE TABLE `db_itss`.`usercard` (
`ID_PseudoBar` varchar(8) NOT NULL,
  `ID_Target` varchar(16) NOT NULL,
  `ID_Code` varchar(16) NOT NULL,
  `ID_Root` int(10),
  `balance` double(10,2) not NULL,
  PRIMARY KEY (`ID_Target`),
  foreign key (`ID_Target`) references target(ID_Ticket),
  foreign key(`ID_Root`) references station(id)
);
CREATE TABLE `db_itss`.`onewayticket`(
`ID_PseudoBar` varchar(8) NOT NULL,
`ID_Target` varchar(16) NOT NULL,
`ID_Code` varchar(16) NOT NULL,
`status` tinyint(1),
`ID_Disembarkation` int(16),
`ID_Embarkation` int(16),
`ID_Real_Start_Station` int(16),
`cost` float,
foreign key (`ID_Disembarkation`) references station(id),
foreign key (`ID_Target`) references target(ID_Ticket),
foreign key (`ID_Embarkation`) references station(id),
foreign key (`ID_Real_Start_Station`) references station(id)
);



INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 1 ,"Saint-Lazare");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 2 ,"Madeleine");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 3 ,"Pyramidese");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 4 ,"Chatelet");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 5 ,"Gare de Lyon");
INSERT INTO `db_itss`.station ( id, name)
   VALUES
   ( 6 ,"Bercy");
INSERT INTO `db_itss`.station ( id, name)
   VALUES
   ( 7 ,"Cour-Saint Emilion");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 8 ,"Bibliotheque Francois Mitterrand");
INSERT INTO `db_itss`.station ( id, name )
   VALUES ( 9 ,"Olympiadese");
--------------------------------------------------------


INSERT INTO `db_itss`.`location` VALUES
 (1,2,5.00),(2,3,3.50),(3,4,2.80),(4,5,4.50),(5,6,3.10),(6,7,3.10),(7,8,3.30),(8,9,3.50),(1,3,8.50),
 (1,4,11.30),(1,5,15.80),(1,6,18.90),(1,7,22.00),(1,8,25.30),(1,9,28.80),(2,4,6.30),(2,5,10.80),
 (2,6,13.90),(2,7,17.00),(2,8,20.30),(2,9,23.80),(3,5,7.30),(3,6,10.40),(3,7,13.50),(3,8,16.80),
 (3,9,20.30),(4,6,7.60),(4,7,10.70),(4,8,14.00),(4,9,17.50),(5,7,6.20),(5,8,9.50),(5,9,13.00),
 (6,8,6.40),(6,9,9.90),(7,9,6.80);
insert into `db_itss`.`target` values ("OW201911260001","e8dc4081b13434b4");
insert into `db_itss`.`target` values ("TF201911260002","07c84c6c4ba59f88");
insert into `db_itss`.`target` values ("PC201911260003","9ac2197d9258257b");
insert into `db_itss`.`target` values ("TF201911260051","57535d2dc95d183c");
insert into `db_itss`.`target` values ("TF201911260052","fd9beab370f72599");

--------------------------------------------------
insert INTO `db_itss`.`onewayticket` values ("abcdefgh","OW201911260001","e8dc4081b13434b4",0,1,3,1,2.7);
insert INTO `db_itss`.`24hticket` values ("ijklmnop","TF201911260002","07c84c6c4ba59f88",now(),2);
insert INTO `db_itss`.`usercard` values ("ABCDEFGH","PC201911260003","9ac2197d9258257b",null,5.56);
insert INTO `db_itss`.`24hticket`(ID_PseudoBar,ID_Target,ID_Code) values ("jfgsdjfr","TF201911260051","57535d2dc95d183c");
insert INTO `db_itss`.`24hticket`(ID_PseudoBar, ID_Target, ID_Code, active_time) values ("zyxbuncd","TF201911260052","fd9beab370f72599","2019-12-01 15:42:43");
