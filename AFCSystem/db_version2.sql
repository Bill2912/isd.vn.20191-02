drop database `db_itss`;

CREATE DATABASE  IF NOT EXISTS `db_itss`;
CREATE TABLE `db_itss`.`station` (
  `id` int(16) NOT NULL,
  `id_zone` int(16),
  `name` varchar(45) not NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `db_itss`.`location` (
  `ID_Station1` int(16)  NOT NULL,
  `ID_Station2` int(16)  Not NULL,
  `distance`  double(10,2) not null,
  foreign key (`ID_Station1`) references station(id),
  foreign key (`ID_Station2`) references station(id)
);
CREATE TABLE `db_itss`.`target` (
  `ID_Ticket` varchar(16) NOT NULL,
  `ID_Code` varchar(16) NOT NULL,
  PRIMARY KEY (`ID_Ticket`)
);
CREATE TABLE `db_itss`.`line`(
`ID_Line` int(16) PRIMARY key not null,
`Routes` varchar(40) not null)
;
CREATE TABLE `db_itss`.`24hticket` (
  `ID_PseudoBar` varchar(8) NOT NULL,
  `ID_Target` varchar(16) NOT NULL,
  `ID_Code`  varchar(16),
  `active_time` datetime ,
  `ID_Root` int(10),
  PRIMARY KEY (`ID_Target`),
  foreign key (`ID_Target`) references target(ID_Ticket)
);
CREATE TABLE `db_itss`.`usercard` (
`ID_PseudoBar` varchar(8) NOT NULL,
  `ID_Target` varchar(16) NOT NULL,
  `ID_Code` varchar(16) NOT NULL,
  `ID_Root` int(10),
  `balance` double(10,2) not NULL,
  PRIMARY KEY (`ID_Target`),
  foreign key (`ID_Target`) references target(ID_Ticket),
  foreign key(`ID_Root`) references station(id)
);
CREATE TABLE `db_itss`.`onewayticket`(
`ID_PseudoBar` varchar(8) NOT NULL,
`ID_Target` varchar(16) NOT NULL,
`ID_Code` varchar(16) NOT NULL,
`status` tinyint(1),
`ID_Disembarkation` int(16),
`ID_Embarkation` int(16),
`ID_Real_Start_Station` int(16),
`cost` float,
`ID_Line` int(16) not null,
foreign key (`ID_Line`) references line(`ID_Line`),
foreign key (`ID_Disembarkation`) references station(id),
foreign key (`ID_Target`) references target(ID_Ticket),
foreign key (`ID_Embarkation`) references station(id),
foreign key (`ID_Real_Start_Station`) references station(id)
);



INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 1 ,"Saint-Lazare");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 2 ,"Madeleine");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 3 ,"Pyramidese");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 4 ,"Chatelet");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 5 ,"Gare de Lyon");
INSERT INTO `db_itss`.station ( id, name)
   VALUES
   ( 6 ,"Bercy");
INSERT INTO `db_itss`.station ( id, name)
   VALUES
   ( 7 ,"Cour-Saint Emilion");
INSERT INTO `db_itss`.station ( id, name )
   VALUES
   ( 8 ,"Bibliotheque Francois Mitterrand");
INSERT INTO `db_itss`.station ( id, name )
   VALUES ( 9 ,"Olympiadese");

update `db_itss`.`station` set station.`id_zone`=1 where station.`id`<5;
update `db_itss`.`station` set station.`id_zone`=2 where station.`id`>=5;

--------------------------------------------------------
insert into `db_itss`.`line` values(1,"1,2,3,4,5,6,7,8,9");
---------------------------------------------------------
INSERT INTO `db_itss`.`location` VALUES
 (1,2,5.00),(2,3,3.50),(3,4,2.80),(4,5,4.50),(5,6,3.10),(6,7,3.10),(7,8,3.30),(8,9,3.50),(1,3,8.50),
 (1,4,11.30),(1,5,15.80),(1,6,18.90),(1,7,22.00),(1,8,25.30),(1,9,28.80),(2,4,6.30),(2,5,10.80),
 (2,6,13.90),(2,7,17.00),(2,8,20.30),(2,9,23.80),(3,5,7.30),(3,6,10.40),(3,7,13.50),(3,8,16.80),
 (3,9,20.30),(4,6,7.60),(4,7,10.70),(4,8,14.00),(4,9,17.50),(5,7,6.20),(5,8,9.50),(5,9,13.00),
 (6,8,6.40),(6,9,9.90),(7,9,6.80);
insert into `db_itss`.`target` values ("OW201911260001","30d0f3619f984b9a");



------------------------------
insert into `db_itss`.`target` values ("OW201911260002","5be17818f9feb2db");
insert into `db_itss`.`target` values ("OW201911260003","80c93ff3372d69b1");
insert into `db_itss`.`target` values ("OW201911260004","3e8adbfb61e7627b");

---------------
insert into `db_itss`.`target` values ("TF201911260051","57535d2dc95d183c");
insert into `db_itss`.`target` values ("TF201911260052","fd9beab370f72599");

insert INTO `db_itss`.`24hticket`(ID_PseudoBar,ID_Target,ID_Code) values ("jfgsdjfr","TF201911260051","57535d2dc95d183c");
insert INTO `db_itss`.`24hticket`(ID_PseudoBar, ID_Target, ID_Code, active_time) values ("zyxbuncd","TF201911260052","fd9beab370f72599","2019-12-01 15:42:43");

------------
insert INTO `db_itss`.`onewayticket` values ("minhdana","OW201911260002","5be17818f9feb2db",0,2,6,3,3.9,1);
insert INTO `db_itss`.`onewayticket` values ("minhdanb","OW201911260003","80c93ff3372d69b1",0,2,9,2,5.9,1);
insert INTO `db_itss`.`onewayticket` values ("minhdanc","OW201911260004","3e8adbfb61e7627b",0,4,8,4,3.5,1);

-------------------------------------------

insert into `db_itss`.`target` values ("TF201911260002","e8dc4081b13434b4");
insert into `db_itss`.`target` values ("PC201911260003","9ac2197d9258257b");
--------------------------------------------------
insert INTO `db_itss`.`onewayticket` values ("abcdefgh","OW201911260001","30d0f3619f984b9a",0,1,3,1,2.7,1);
insert INTO `db_itss`.`24hticket`(`ID_PseudoBar`,`ID_Target`,`ID_Code`,`active_time`) values ("ijklmnop","TF201911260002","e8dc4081b13434b4",now());
insert INTO `db_itss`.`usercard` values ("ABCDEFGH","PC201911260003","9ac2197d9258257b",null,100);


--------------------
--------------------
insert into `db_itss`.`target` values ("TF201912030001","b4306a71e5d92737");
insert into `db_itss`.`target` values ("TF201912030002","dc8e5079675f1ca7");
insert into `db_itss`.`target` values ("TF201912030003","407600a834116a5f");
--------
insert INTO `db_itss`.`24hticket` values ("diepdiep","TF201912030001","b4306a71e5d92737","2019-12-02 09:30:03",2);
insert INTO `db_itss`.`24hticket` values ("diepabcd","TF201912030002","dc8e5079675f1ca7","2019-12-09 19:30:03",2);
insert INTO `db_itss`.`24hticket`(`ID_PseudoBar`,`ID_Target`,`ID_Code`) values ("diephhhh","TF201912030003","407600a834116a5f");
