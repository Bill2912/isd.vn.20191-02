*Subject:  Software Development  
Lecturer: Prof. Nguyen Thi Thu Trang  *
# Repository :ISD.VN.20191-02
## Home-Work11:  Report code about SOLID
##      Team 02:
*    Nguyễn Minh Dân (Leader)
*    Phạm Việt Cường
*    Đặng Ngọc Diệp
*    Phùng Thành Công 
##    Folder's Team 
1.	Controller - Chứa các logic của chương trình
1.	Model - Chứa class entity ứng với cơ sở dữ liệu
1.	Repository - Chứa các truy vấn của chương trình
1.	Service - Là các interface phục vụ cho package controller
1.	View - Chứa giao diện của hệ thống
1.	AFCSystemApllication - class thực thi của hệ thống
##    Work Assignment:
1. Dân viết code và test phần one way ticket
1. Công viết code và test phần prepaid card, giao diện
1. Diệp viết code và test phần 24h ticket
1. Cường viết code và test phần truy vấn dữ liệu, entity
