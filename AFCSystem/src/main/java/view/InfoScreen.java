package view;

import controller.CardController;
import controller.TicketOWController;
import controller.TicketTFController;
import model.TicketOWEntity;
import model.TicketTFEntity;
import model.UserCardEntity;
import util.ControlGate;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

public class InfoScreen {
    private static InfoScreen infoScreen;

    private InfoScreen() {
    }

    private static CardController cardController = new CardController();
    private static TicketOWController owController = new TicketOWController();
    private static TicketTFController tfController = new TicketTFController();

    /**
     * print message card when passenger enter
     * @param userCardEntity is an object
     * @param idStation      get id station when passenger enter
     */
    public static void messageCardEnter(UserCardEntity userCardEntity, int idStation) {
        String result = cardController.requestValidationEnter(userCardEntity, idStation);
        ControlGate.controlGateFromValid(result);
        System.out.println(result + "\n" + userCardEntity.display());
    }
    /**
     * print message card when passenger exit
     * @param userCardEntity is an object
     * @param idStation      get id station when passenger exit
     */
    public static void messageCardExit(UserCardEntity userCardEntity, int idStation) {
        String result = cardController.requestValidationExit(userCardEntity, idStation);
        ControlGate.controlGateFromValid(result);
        PrintMessage.printMessageCard(result);
        System.out.println(result + "\n" + userCardEntity.display());
    }

    /**
     * print message one way when passenger enter
     *
     * @param owEntity  is an object
     * @param idStation get id station when passenger enter
     */
    public static void messageOWEnter(TicketOWEntity owEntity, int idStation) {
        String result = owController.requestValidationEnter(owEntity, idStation);
        ControlGate.controlGateFromValid(result);
        PrintMessage.printMessageOW(result);
        System.out.println(result + "\n" + owEntity.display());
    }

    /**
     * print message one way when passenger exit
     *
     * @param owEntity  is an object
     * @param idStation get id station when passenger exit
     */
    public static void messageOWExit(TicketOWEntity owEntity, int idStation) {
        String result = owController.requestValidationExit(owEntity, idStation);
        ControlGate.controlGateFromValid(result);
        PrintMessage.printMessageOW(result);
        System.out.println(result + "\n" + owEntity.display());
    }

    /**
     * print message 24h when passenger enter
     *
     * @param tfEntity  is an object
     * @param idStation get id station when passenger enter
     */
    public static void messageTFEnter(TicketTFEntity tfEntity, int idStation) {
        String result = tfController.requestValidationEnter(tfEntity, idStation);
        ControlGate.controlGateFromValid(result);
        PrintMessage.printMessageTF(result);
        System.out.println(result + "\n" + tfEntity.display(1));
    }

    /**
     * print message 24h when passenger exit
     *
     * @param tfEntity  is an object
     * @param idStation get id station when passenger exit
     */
    public static void messageTFExit(TicketTFEntity tfEntity, int idStation) {
        String result = tfController.requestValidationExit(tfEntity, idStation);
        ControlGate.controlGateFromValid(result);
        PrintMessage.printMessageTF(result);
        System.out.println(result + "\n" + tfEntity.display(2));
    }
}
