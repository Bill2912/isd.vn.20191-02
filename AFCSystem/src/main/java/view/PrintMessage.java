package view;

import static util.Constant.*;

public class PrintMessage {
    private static PrintMessage printMessage;

    private PrintMessage() {
    }

    public static void printMessageCard(String result) {
        if (result.equals(REQUEST_OPEN_GATE)) {
            System.out.println(VALID_CARD);
        } else if (result.equals(REQUEST_CLOSE_GATE)) {
            System.out.println(INVALID_CARD);
        }
    }

    public static void printMessageTF(String result) {
        if (result.equals(REQUEST_OPEN_GATE)) {
            System.out.println(VALID_TICKET_24H);
        } else if (result.equals(REQUEST_CLOSE_GATE)) {
            System.out.println(INVALID_TICKET_24H);
        }
    }

    public static void printMessageOW(String result) {
        if (result.equals(REQUEST_OPEN_GATE)) {
            System.out.println(VALID_TICKET_ONE_WAY);
        } else if (result.equals(REQUEST_CLOSE_GATE)) {
            System.out.println(INVALID_TICKET_ONE_WAY);
        }
    }
}
