package view;

import controller.CardController;
import controller.TicketOWController;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;
import model.*;
import service.*;
import util.SplitString;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AFCScreen {
    private static AFCScreen afcScreen;

    private AFCScreen() {
    }

    private static StationService stationService = new StationServiceImp();
    private static UserCardService userCardService = new UserCardServiceImp();
    private static TicketOWService ticketOWService = new TicketOWServiceImp();
    private static TicketTFService ticketTFService = new TicketTFServiceImp();
    private static CardScanner cardScanner = CardScanner.getInstance();
    private static TargetService targetService = new TargetServiceImp();
    private static Scanner in = new Scanner(System.in);
    private static TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();

    /**
     * print main screen
     */
    public static void MainScreen() {
        List<StationEntity> stationEntities = stationService.getAll();
        System.out.println("These are stations in the line M14 of Paris:");
        for (StationEntity stationEntity : stationEntities) {
            System.out.println(stationEntity.getId() + ". " + stationEntity.getName());
        }
        System.out.println("Available actions: 1 -enter station, 2-exit station");
        System.out.println("You can provide a combination of number (1 or 2) and a letter from (1 to 9) to enter or exit\n" +
                "a station (using hyphen in between). For instance, the combination “2-6” will bring you to\n" +
                "exit the station Chatelet.:");
        System.out.println("Your input: ");
    }

    /**
     * print choose ticket/card screen
     */
    public static void chooseTicketCardScreen() {
        System.out.println("These are existing tickets/cards:");
        List<UserCardEntity> userCardEntities = userCardService.getAll();
        List<TicketOWEntity> oneWayTicketEntities = ticketOWService.getAll();
        List<TicketTFEntity> ticket24hEntities = ticketTFService.getAll();

        for (TicketTFEntity ticketTFEntity : ticket24hEntities) {
            System.out.println("\t" + ticketTFEntity.getPseudoBar() + ": 24h tickets: Valid until "
                    + ticketTFEntity.getActive_time());
        }
//        for (TicketOWEntity oneWayTicketEntity : oneWayTicketEntities) {
//            System.out.println("\t" + oneWayTicketEntity.getPseudoBar() + ": One-way ticket between "
//                    + oneWayTicketEntity.getEmbarkation() + " and " + oneWayTicketEntity.getDisembarkation() + ": -"
//                    + oneWayTicketEntity.getCost() + " euros");
//        }
                for (TicketOWEntity oneWayTicketEntity : oneWayTicketEntities) {
            System.out.println("\t" + oneWayTicketEntity.getPseudoBar() + ": One-way ticket between "
                    + oneWayTicketEntity.getDisembarkation() + " and " + oneWayTicketEntity.getEmbarkation() + ": "
                    + oneWayTicketEntity.getCost() + " euros");
        }

        for (UserCardEntity userCardEntity : userCardEntities) {
            System.out.println("\t" + userCardEntity.getPseudoBar() + ": Prepaid Card: "
                    + userCardEntity.getBalance() + " euros");
        }
        System.out.println("Please provide the ticket code you want to enter/exit:");
    }

    /**
     * Press Enter Key to continue
     */
    private static void pressAnyKeyToContinue() {
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        } catch (Exception e) {
        }
    }

    /**
     * Check object null
     *
     * @param obj is an object
     * @return object null
     */
    private static boolean isNull(Object obj) {
        return obj == null;
    }

    /**
     * User use step by step this method
     */
    public static void userScreen() {
        do {
            AFCScreen.MainScreen();
            String s = in.nextLine();
            if (s.trim().equals("")) {
                System.out.println("Ban nhap chuoi rong, xin vui long nhap lai.");
            }
            try {
                ArrayList<String> string = SplitString.splitInput(s,"-");
                int ch = Integer.parseInt(string.get(0));
                int stationId = Integer.parseInt(string.get(1));
                switch (ch) {
                    case 1:
                        AFCScreen.chooseTicketCardScreen();
                        String pseudoBarCode = in.nextLine();
                        if (pseudoBarCode.length() == 8) {
                            if (pseudoBarCode.equals(pseudoBarCode.toUpperCase()) == true) {
                                try {
                                    String cardId = cardScanner.process(pseudoBarCode);
                                    TargetEntity targetEntity = targetService.getTargetbyId(cardId);
                                    if (AFCScreen.isNull(targetEntity) == false) {
                                        UserCardEntity userCardEntity = userCardService.getById(targetEntity.getIdTicket());
                                        InfoScreen.messageCardEnter(userCardEntity, stationId);
                                    } else {
                                        System.out.println("Can't found");
                                    }
                                } catch (InvalidIDException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    String ticketId = ticketRecognizer.process(pseudoBarCode);
                                    TargetEntity targetEntity = targetService.getTargetbyId(ticketId);
                                    if (AFCScreen.isNull(targetEntity) == false) {
                                        String idTicket = targetEntity.getIdTicket();
                                        System.out.println(idTicket);
                                        boolean ow = idTicket.startsWith("OW");
                                        if (ow == true) {
                                            //24h request validation
                                            TicketOWEntity owEntity = ticketOWService.getTicketbyId(idTicket);
                                            InfoScreen.messageOWEnter(owEntity, stationId);


                                        } else {
                                            TicketTFEntity tfEntity = ticketTFService.getById(idTicket);
                                            InfoScreen.messageTFEnter(tfEntity, stationId);

                                        }
                                    } else {
                                        System.out.println("Can't found");
                                    }
                                } catch (InvalidIDException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            System.out.println("Ban hay nhap du 8 ki tu");
                        }
                        break;
                    case 2:
                        AFCScreen.chooseTicketCardScreen();
                        String pseudoCode = in.nextLine();
                        if (pseudoCode.length() == 8) {
                            if (pseudoCode.equals(pseudoCode.toUpperCase()) == true) {
                                try {
                                    String cardId = cardScanner.process(pseudoCode);
                                    TargetEntity targetEntity = targetService.getTargetbyId(cardId);
                                    if (AFCScreen.isNull(targetEntity) == false) {
                                        UserCardEntity userCardEntity = userCardService.getById(targetEntity.getIdTicket());
                                        InfoScreen.messageCardExit(userCardEntity, stationId);
                                    } else {
                                        System.out.println("Can't found");
                                    }
                                } catch (InvalidIDException e) {
                                    e.printStackTrace();
                                }
                            } else {

                                try {
                                    String ticketId = ticketRecognizer.process(pseudoCode);
                                    TargetEntity targetEntity = targetService.getTargetbyId(ticketId);
                                    if (AFCScreen.isNull(targetEntity) == false) {
                                        String idTicket = targetEntity.getIdTicket();

                                        boolean ow = idTicket.startsWith("OW");
                                        if (ow == true) {
                                            TicketOWEntity owEntity = ticketOWService.getTicketbyId(idTicket);
                                            InfoScreen.messageOWExit(owEntity, stationId);

                                        } else {
                                            TicketTFEntity tfEntity = ticketTFService.getById(idTicket);
                                            InfoScreen.messageTFExit(tfEntity, stationId);
                                        }
                                    } else {
                                        System.out.println("Can't found");
                                    }
                                } catch (InvalidIDException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            System.out.println("Ban hay nhap du 8 ki tu");
                        }
                        break;
                }
            } catch (Exception ex) {
                System.out.println("Ban nhap chuoi khong hop le, xin vui long nhap lai");
            }
            AFCScreen.pressAnyKeyToContinue();
        } while (true);
    }
}
