package util;

import hust.soict.se.gate.Gate;
import static util.Constant.*;
public class ControlGate {
    private static ControlGate controlGate;
    private ControlGate(){}
    private  static Gate gate = Gate.getInstance();

    public static void controlGateFromValid(String result){
        if (result.equals(REQUEST_OPEN_GATE)) {
            gate.open();
        }
        else if(result.equals(REQUEST_CLOSE_GATE)){
            gate.close();
        }
    }
}
