package util;

import model.LocationEntity;
import repository.LocationRepository;
import service.LocationService;
import service.LocationServiceImp;

public class GetDistance {
	public double getDistanceByID(int id_station1 , int id_station2) {
		LocationService lc = new LocationServiceImp();
		LocationEntity location= lc.getById(id_station1, id_station2);
		return location.getDistance();
	}
}
