package util;

public interface Calculate {

    double calculateAmount(double distance);

}
