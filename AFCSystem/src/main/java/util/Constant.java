package util;

public class Constant {
    public static final double FARE_BASE = 1.9;
    public static final double NEXT_TO_2KM = 0.4;
    public static final String REQUEST_OPEN_GATE = "Open Gate";
    public static final String REQUEST_CLOSE_GATE = "Close Gate";
    public static final String INVALID_CARD = "Balance in card not enough";
    public static final String INVALID_TICKET_24H = "Ticket has expired";
    public static final String INVALID_TICKET_ONE_WAY = "The ticket is not valid";
    public static final String VALID_CARD = "Opening card";
    public static final String VALID_TICKET_24H = "Opening ticket 245";
    public static final String VALID_TICKET_ONE_WAY = "Opening ticket one way";
}
