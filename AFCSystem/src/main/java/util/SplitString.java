package util;

import java.util.ArrayList;
import java.util.Arrays;

public class SplitString {
    private static SplitString splitString;
    private SplitString(){}
    public static ArrayList<String> splitInput(String input,String chracter){
        return new ArrayList<>(Arrays.asList(input.split(chracter)));
    }

}
