package util;

import static util.Constant.FARE_BASE;
import static util.Constant.NEXT_TO_2KM;

public class CalculateAmountby2km implements Calculate {
    @Override
    public double calculateAmount(double distance) {
        double amount = 0.0;
        double calculateDistance = Math.ceil((distance - 5) / 2);
        if ((distance <= 5.0) && (distance > 0)) {
            amount = FARE_BASE;
        } else {
            amount = (double) (FARE_BASE + NEXT_TO_2KM * calculateDistance);
        }
        return amount;
    }


}
