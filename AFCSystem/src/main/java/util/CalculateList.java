package util;

public class CalculateList {
    private Calculate calculate;

    public void setCalculate(Calculate calculate) {
        this.calculate = calculate;
    }
    public double calculate(double distance){
        return calculate.calculateAmount(distance);
    }

}
