package repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import model.TargetEntity;
import service.HibernateUtils;

public class TargetRepository {
	public List<TargetEntity> getAll(Session ses){
		List<TargetEntity> list = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ TargetEntity.class.getSimpleName() +" p";
			
			Query<TargetEntity> query = ses.createQuery(sql);
			
			list = query.list();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } 
		return list;
	}
	
	public TargetEntity getById(String idCode, Session ses) {
		TargetEntity target = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ TargetEntity.class.getSimpleName() +" p "
    				+ "Where p.idCode =:id ";
			
			Query<TargetEntity> query = ses.createQuery(sql);
			query.setParameter("id", idCode);
			
			target = query.uniqueResult();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return target;
	}
}
