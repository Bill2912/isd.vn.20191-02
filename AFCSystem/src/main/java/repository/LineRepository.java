package repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import model.LineEntity;
import model.TicketOWEntity;


public class LineRepository {
	public LineEntity getById(int id, Session ses) {
		LineEntity loc = null;
    	if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
    		ses.getTransaction().begin();
		}
    	
    	try {
			String sql = "Select p From "+ LineEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
    		
    		Query<LineEntity> query = ses.createQuery(sql);
			query.setParameter("id", id);
			
			loc = query.uniqueResult();
	         
    	}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
    	return loc;
    }

    public List<LineEntity> getAll(Session ses) {
		List<LineEntity> stations = null;
		
    	if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
    		ses.getTransaction().begin();
		}
    	
		try	{	
			
			String sql = "Select p From "+ LineEntity.class.getSimpleName() +" p";
			
			Query<LineEntity> query = ses.createQuery(sql);
			
			stations = query.list();
			
		} catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		
		return stations;
	}
}
