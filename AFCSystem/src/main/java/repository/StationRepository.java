package repository;

import model.StationEntity;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.List;

public class StationRepository {
	
	public List<StationEntity> getAll(Session ses){
		List<StationEntity> list = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ StationEntity.class.getSimpleName() +" p";
			
			Query<StationEntity> query = ses.createQuery(sql);
			
			list = query.list();
		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		
		return list;
	}
	
	public StationEntity getByName(String name, Session ses) {
		StationEntity station = null;

		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ StationEntity.class.getSimpleName() +" p "
    				+ "Where p.name =:name ";
			
			Query<StationEntity> query = ses.createQuery(sql);
			query.setParameter("name", name);
			
			station = query.uniqueResult();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return station;
	}
	
	public StationEntity getById(int id, Session ses) {
		StationEntity station = null;

		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ StationEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
			
			Query<StationEntity> query = ses.createQuery(sql);
			query.setParameter("id", id);
			
			station = query.uniqueResult();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return station;
	}
	
}
