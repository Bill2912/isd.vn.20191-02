package repository;

import model.LocationEntity;
import service.HibernateUtils;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;


public class LocationRepository {

    public LocationEntity getLocationbyID(int id_station1, int id_station2, Session ses) {
    	LocationEntity loc = null;
    	if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
    		ses.getTransaction().begin();
		}
    	
    	try {
    		String sql = "Select p From "+ LocationEntity.class.getSimpleName() +" p "
    				+ "Where p.pk.station1.id =:stationId1 "
    				+ "And p.pk.station2.id =:stationId2 ";
    		
    		Query<LocationEntity> query = ses.createQuery(sql);
			query.setParameter("stationId1", id_station1);
			query.setParameter("stationId2", id_station2);
			
			loc = query.uniqueResult();
	         
    	}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
    	return loc;
    }

    public List<LocationEntity> getLocation(Session ses) {
		List<LocationEntity> stations = null;
		
    	if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
    		ses.getTransaction().begin();
		}
    	
		try	{	
			
			String sql = "Select p From "+ LocationEntity.class.getSimpleName() +" p";
			
			Query<LocationEntity> query = ses.createQuery(sql);
			
			stations = query.list();
			
		} catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		
		return stations;
	}
}