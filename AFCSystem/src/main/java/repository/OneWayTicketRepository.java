package repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import model.TicketOWEntity;

public class OneWayTicketRepository {
	
	public List<TicketOWEntity> getAll(Session ses){
		List<TicketOWEntity> tickList = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			
			String sql = "Select p From "+ TicketOWEntity.class.getSimpleName() +" p";
			
			Query<TicketOWEntity> query = ses.createQuery(sql);
			
			tickList = query.list();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return tickList;
	}
	
	public TicketOWEntity getById(String id, Session ses) {
		TicketOWEntity ticket = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ TicketOWEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
			
			Query<TicketOWEntity> query = ses.createQuery(sql);
			query.setParameter("id", id);
			
			ticket = query.uniqueResult();

		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		
		return ticket;
	}
	
	public void setStatus(String ticke_id, int status, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {			
			
			String sql = "Select p From "+ TicketOWEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
			
			Query<TicketOWEntity> query = ses.createQuery(sql);
			query.setParameter("id", ticke_id);
			TicketOWEntity ticket = query.uniqueResult();
			ticket.setStatus(status);
			
			ses.flush();
			
		}catch (Exception e){
			e.printStackTrace();
			ses.getTransaction().rollback();
		}
		
	}
	
	public void setRootId(String ticke_id, int rootid, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try	{
			String sql = "Select p From "+ TicketOWEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
			
			Query<TicketOWEntity> query = ses.createQuery(sql);
			query.setParameter("id", ticke_id);
			
			TicketOWEntity ticket = query.uniqueResult();
			ticket.setReal_station_start(rootid);
			
			ses.flush();
		}catch (Exception e){
			e.printStackTrace();
			ses.getTransaction().rollback();
		}
	}
}
