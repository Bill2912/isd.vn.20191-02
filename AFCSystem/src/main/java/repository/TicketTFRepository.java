package repository;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import model.TicketTFEntity;

import java.time.LocalDateTime;
import java.util.List;

public class TicketTFRepository {
	public List<TicketTFEntity> getAll(Session ses){
		List<TicketTFEntity> list = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ TicketTFEntity.class.getSimpleName() +" p";
			
			Query<TicketTFEntity> query = ses.createQuery(sql);
			
			list = query.list();
		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return list;
	}
	
	public TicketTFEntity getById(String id, Session ses) {
		TicketTFEntity target = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ TicketTFEntity.class.getSimpleName() +" p "
    				+ "Where p.id =:id ";
			
			Query<TicketTFEntity> query = ses.createQuery(sql);
			query.setParameter("id", id);
			
			target = query.uniqueResult();
		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return target;
	}

	public void setRootId(String targetId, Integer rootId, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		TicketTFEntity card = this.getById(targetId, ses);
		card.setRootId(rootId);
		ses.flush();
		
	}
	
	public void updateTime(String ticketid, LocalDateTime active_time, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		TicketTFEntity tick = this.getById(ticketid, ses);
		
		tick.setActive_time(active_time);
		ses.flush();
	}
}

