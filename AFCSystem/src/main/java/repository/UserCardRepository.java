package repository;

import java.util.List;

import model.UserCardEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

public class UserCardRepository {
	
	public List<UserCardEntity> getAll(Session ses){
		List<UserCardEntity> list = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ UserCardEntity.class.getSimpleName() +" p";
			
			Query<UserCardEntity> query = ses.createQuery(sql);
			
			list = query.list();
		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return list;
	}
	
	public UserCardEntity getById(String targetId, Session ses) {
		UserCardEntity target = null;
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		try {
			String sql = "Select p From "+ UserCardEntity.class.getSimpleName() +" p "
    				+ "Where p.targetId =:targetId ";
			
			Query<UserCardEntity> query = ses.createQuery(sql);
			query.setParameter("targetId", targetId);
			
			target = query.uniqueResult();
		}catch(Exception sqlException) {
            if(ses.getTransaction() != null) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                ses.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        }
		return target;
	}
	
	public void setRootId(String targetId, Integer rootId, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		UserCardEntity card = this.getById(targetId, ses);
		card.setRootId(rootId);
		ses.flush();
		
	}

	public void setBalance(String targetId, double balance, Session ses) {
		if (ses.getTransaction().getStatus() == TransactionStatus.NOT_ACTIVE) {
			ses.beginTransaction();
		}
		UserCardEntity card = this.getById(targetId, ses);
		card.setBalance(balance);
		ses.flush();
		
	}
}
