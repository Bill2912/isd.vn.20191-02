import controller.TicketOWController;
import model.LineEntity;
import model.TicketOWEntity;
import service.LineService;
import service.LineServiceImp;
import service.TicketOWService;
import service.TicketOWServiceImp;
import util.SplitString;
import java.lang.System;
import java.util.ArrayList;
import java.util.List;

public class test {
    public static void main(String[] args) {
        TicketOWService ticketOWService = new TicketOWServiceImp();
        TicketOWEntity ticket = ticketOWService.getTicketbyId("OW201911260002");
        System.out.println(ticket.display());
       // System.out.println(ticket.getDisembarkation());
       // System.out.println(ticket.getEmbarkation());
        LineService lineService = new LineServiceImp();
        LineEntity line1 = lineService.getById(ticket.getIdLine());
        List<String> arrayRoutes = SplitString.splitInput(line1.getRoutes(),",");
        System.out.println(arrayRoutes);
        int dauvao = getIndex(arrayRoutes,ticket.getDisembarkation());
        int daura = getIndex(arrayRoutes,ticket.getEmbarkation());
        System.out.println(dauvao +"-" + daura);

        TicketOWController ticketOWController = new TicketOWController();
        ticketOWController.UpdateRealStation(ticket,5);
        System.out.println("SAU khi thay doi trong enity " + ticket.getReal_station_start());
        TicketOWEntity ticket2 = ticketOWService.getTicketbyId("OW201911260002");
        System.out.println( "load csdl "+ticket2.getReal_station_start());
        ticketOWController.UpdateStatus(ticket,6);
        System.out.println("SAU khi thay doi trong enity " + ticket.getStatus());
        System.out.println( "load csdl "+ticket2.getStatus());


    }

    public static Integer getIndex(List<String> array , int id){
        int i=0;
        int value;
        for(;i<array.size();i++){
            value=Integer.parseInt(array.get(i));
            if(value==id)
                break;
        }
        return i+1;
    }


}
