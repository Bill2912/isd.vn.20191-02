package controller;
public abstract class TargetController<T> {

    public abstract String requestValidationEnter(T t,int stationId);
    public abstract String requestValidationExit(T t,int exitId);


}
