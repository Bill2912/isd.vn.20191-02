package controller;

import hust.soict.se.gate.Gate;
import model.LocationEntity;
import model.UserCardEntity;
import service.*;
import util.Calculate;
import util.CalculateAmountby2km;
import util.CalculateList;

import static util.Constant.*;

public class CardController extends TargetController<UserCardEntity> {
    private CalculateList calculateList = new CalculateList();

    private UserCardService userCardService = new UserCardServiceImp();
    private LocationService locationService = new LocationServiceImp();

    /**
     * compare balance of user card with amount after calculate
     *
     * @param balance  of user card
     * @param distance of two station(get from database)
     * @return false if balance < amount , true if balance >= amount
     */
    private boolean compareBalancewithAmount(double balance, double distance) {
        double amount = 0.0;
        calculateList.setCalculate(new CalculateAmountby2km());
        amount = calculateList.calculate(distance);
        if (balance < amount) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * compare balance of user card with fare base
     *
     * @param balance of user card
     * @return false if balance < fare_base, true if balance >= fare_base
     */
    private boolean compareBalancewithFareBase(double balance) {
        if (balance < FARE_BASE) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * this method use in unit test
     *
     * @param amount of two station
     * @return string
     */
    public String displayAmount(double amount) {

        calculateList.setCalculate(new CalculateAmountby2km());
        return "so tien la:" + calculateList.calculate(amount);
    }

    /**
     * update id station when passenger enter station into database
     *
     * @param userCardEntity is an object
     * @param stationId      get id of station when passenger enter station
     * @return void
     */
    private void setRootId(UserCardEntity userCardEntity, int stationId) {
        userCardService.updateRootId(userCardEntity.getTargetId(), stationId);
    }

    /**
     * update new balance into database
     *
     * @param userCardEntity is an object
     * @param newBalance     balance after calculate
     * @return void
     */
    private void setNewBalance(UserCardEntity userCardEntity, double newBalance) {
        userCardService.updateBalance(userCardEntity.getTargetId(), newBalance);
    }


    /**
     * check condition when passenger enter station and return a String to open/close gate
     *
     * @param userCardEntity is an object
     * @param stationId      get id station when passenger enter
     * @return Opening gate if check true, Close gate if check false
     */
    @Override
    public String requestValidationEnter(UserCardEntity userCardEntity, int stationId) {
        boolean check = this.compareBalancewithFareBase(userCardEntity.getBalance());
        if (check == true) {
            this.setRootId(userCardEntity, stationId);
            return REQUEST_OPEN_GATE;
        } else {
            return REQUEST_CLOSE_GATE;
        }
    }

    /**
     * check condition when passenger exit station and return a String to open/close gate
     *
     * @param userCardEntity is an object
     * @param exitId         get id station when passenger exit
     * @return Opening gate if check true, Close gate if check false
     */
    @Override
    public String requestValidationExit(UserCardEntity userCardEntity, int exitId) {
        LocationEntity locationEntity = locationService.getById(userCardEntity.getRootId(), exitId);

        boolean check = this.compareBalancewithAmount(userCardEntity.getBalance(), locationEntity.getDistance());
        if (check == true) {
            calculateList.setCalculate(new CalculateAmountby2km());

            double newBalance = this.calculateNewBalance(userCardEntity.getBalance(),
                    calculateList.calculate(locationEntity.getDistance()));
            userCardEntity.setBalance(newBalance);
            this.setNewBalance(userCardEntity, newBalance);
            return REQUEST_OPEN_GATE;
        } else {
            return REQUEST_CLOSE_GATE;
        }
    }

    /**
     * calculate new balance
     *
     * @param balance is current balance
     * @param amount  is the amount guest must pay
     * @return double
     */
    public double calculateNewBalance(double balance, double amount) {
        return balance - amount;
    }

}
