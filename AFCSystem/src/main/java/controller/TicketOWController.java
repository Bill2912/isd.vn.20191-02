package controller;
import model.TicketOWEntity;
import service.TicketOWService;
import service.TicketOWServiceImp;
import service.LineService;
import  service.LineServiceImp;
import util.*;
import model.LineEntity;
import static util.Constant.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class TicketOWController extends TargetController<TicketOWEntity> {
	TicketOWService ticketOWService = new TicketOWServiceImp();

	/** check ticket is actived or active 1: actived , 0 : not active
	 * @param ticket : One Way Ticket Enity
	 * @return if ticket actived return false , else return true
	 */
	public Boolean CheckStatus (TicketOWEntity ticket) {
		if(ticket.getStatus()==1)
			return false;
		else
			return true;
	}

	/**	The station will be suitable if it belongs to the station between 2 stations in a route
	 * @param ticket
	 * @param id_station
	 * @return true if Station fit else false
	 */
	public Boolean CheckStation(TicketOWEntity ticket, int id_station) {
		LineService lineService = new LineServiceImp();
		LineEntity line1 = lineService.getById(ticket.getIdLine());
		ProcessArray processArray = new ProcessArray();
		List<String> routes = SplitString.splitInput(line1.getRoutes(),",");
		int disembarkation = processArray.getIndex(routes,ticket.getDisembarkation());
		int embarkation = processArray.getIndex(routes,ticket.getEmbarkation());
		int index_station = processArray.getIndex(routes,id_station);
		if(index_station>=disembarkation&&index_station<=embarkation)
			return true;
		else
			return false;
	}

	/**Check the balance when customers get off the train
	 * @param ticket
	 * @param id_station
	 * @return if cost >= balance return true else return false
	 */
	public Boolean CheckBalance(TicketOWEntity ticket, int id_station) {
		GetDistance getDistance = new GetDistance();
		double distance = getDistance.getDistanceByID(ticket.getReal_station_start(),id_station);
		CalculateList calculateList = new CalculateList();
		calculateList.setCalculate(new CalculateAmountby2km());
		double price = calculateList.calculate(distance);
		if(price<=ticket.getCost())
			return true;
		else
			return false;
	}
	/** if customers exit successful ticket is actived
	 * @param ticket
	 * @param id_station
	 */
	public void UpdateStatus(TicketOWEntity ticket, int id_station) {
		if(requestValidationExit(ticket, id_station)==REQUEST_OPEN_GATE) {
			ticket.setStatus(1);
			ticketOWService.updateStatus(ticket.getId(), 1);
		}
	}

	/**if customers enter station successful update real_staion_start
	 * @param ticket
	 * @param id_station
	 */
	public void UpdateRealStation (TicketOWEntity ticket, int id_station) {
		if(requestValidationEnter(ticket, id_station)==REQUEST_OPEN_GATE)
		{	//update entity
			ticket.setReal_station_start(id_station);
			//update database
			ticketOWService.setRealStationStart(ticket.getId(),id_station);
//			oneWayTicketService.updatRealStationStart(ticket.getId(),id_station)
//
		}
	}
	/**  check status and check station if customers enter station
	 * @param ticketOWEntity
	 * @param stationId
	 * @return true if status and station true else false
	 */
	@Override
	public String requestValidationEnter(TicketOWEntity ticketOWEntity, int stationId) {
		if(CheckStatus(ticketOWEntity)==false)
			return REQUEST_CLOSE_GATE;
		else if(CheckStation(ticketOWEntity,stationId)==false)
			return REQUEST_CLOSE_GATE;
		else{
			ticketOWEntity.setReal_station_start(stationId);
			ticketOWService.setRealStationStart(ticketOWEntity.getId(),stationId);
			return REQUEST_OPEN_GATE;
		}
	}
	/** check status and balance if customers exit station
	 * if exit succs
	 * @param ticketOWEntity
	 * @param exitId
	 * @return REQUEST_CLOSE_GATE if checkstatus and checkbalance false, else true
	 */
	@Override
	public String requestValidationExit(TicketOWEntity ticketOWEntity, int exitId) {
		if(CheckStatus(ticketOWEntity)==false)
			return REQUEST_CLOSE_GATE;
		else if(CheckBalance(ticketOWEntity,exitId)==false)
			return REQUEST_CLOSE_GATE;
		else{
			ticketOWService.updateStatus(ticketOWEntity.getId(), 1);
			ticketOWEntity.setStatus(1);
			return REQUEST_OPEN_GATE;
		}
	}
}

