package controller;
import hust.soict.se.gate.Gate;
import model.TicketTFEntity;
import service.TicketTFService;
import service.TicketTFServiceImp;
import java.time.LocalDateTime;
import static util.Constant.*;
public class TicketTFController extends TargetController<TicketTFEntity> {
	private TicketTFService ticket24hservice = new TicketTFServiceImp();
	public TicketTFController(){

	}
	public TicketTFController(TicketTFService ticket24hservice){
		this.ticket24hservice = ticket24hservice;
	}

	/**
	 * check if this ticket is active or not
	 * @param active_time get active_time of ticket
	 * @return True if ticket is active, False if ticket is not active
	 */
	private boolean checkActiveTicket(LocalDateTime active_time){
		return active_time != null;
	}

	/**
	 * check if this ticket has time left less than 24 hours or not
	 * @param active_time get active_time of ticket
	 * @param datetime time when check
	 * @return True if ticket has time left less than 24 hours, False if not
	 */
	private boolean checkTimeLeft(LocalDateTime active_time, LocalDateTime datetime){
		return datetime.isBefore(active_time.plusDays(1));
	}

	/**
	 * check condition when passenger enter station and return a String to open/close gate
	 * @param ticketTFEntity is an object
	 * @param rootId get id station when passenger enter
	 * @return Opening gate if check true, Close gate if check false
	 */
	@Override
	public String requestValidationEnter(TicketTFEntity ticketTFEntity, int rootId){
		LocalDateTime datetime = LocalDateTime.now();
		if (ticketTFEntity.getRootId() != null){
			return REQUEST_CLOSE_GATE;
		}else{
			if(this.checkActiveTicket(ticketTFEntity.getActive_time())){
				if (this.checkTimeLeft(ticketTFEntity.getActive_time(), datetime)){
					ticket24hservice.updateRootId(ticketTFEntity.getId(), rootId);
					return REQUEST_OPEN_GATE;
				}
				else{
					return REQUEST_CLOSE_GATE;
				}
			}
			else{
				ticket24hservice.updateTime(ticketTFEntity.getId(), datetime );
                ticket24hservice.updateRootId(ticketTFEntity.getId(), rootId);
				return REQUEST_OPEN_GATE;
			}
		}
	}

	/**
	 * check condition when passenger exit station and return a String to open/close gate
	 * @param ticketTFEntity is an object
	 * @param rootId get id station when passenger exit
	 * @return Opening gate if check true, Close gate if check false
	 */
	@Override
	public String requestValidationExit(TicketTFEntity ticketTFEntity, int rootId){
		if (ticketTFEntity.getRootId() != null){
			ticket24hservice.updateRootId(ticketTFEntity.getId(), null);
			return REQUEST_OPEN_GATE;
		}
		else{
			return REQUEST_CLOSE_GATE;
		}
	}
}
