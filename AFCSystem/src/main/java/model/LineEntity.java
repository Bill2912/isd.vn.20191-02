package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Clock;

@Entity
@Table(name = "line")

public class LineEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_Line")
	private int id;

	@Column(name = "Routes")
	private String routes;
	
	public String getRoutes() {
		return routes;
	}

	public void setRoutes(String routes) {
		this.routes = routes;
	}

	public int getId() {
		return id;
	}

	public LineEntity() {
		super();
	}

	public LineEntity(String routes) {
		super();
		this.routes = routes;
	}
	public  void display(){
		System.out.println("LINE :" +this.getId() + "\n" + this.getRoutes());
	}

}
