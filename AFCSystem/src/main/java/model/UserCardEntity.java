package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "usercard")
public class UserCardEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_Target", nullable = false)
    private String targetId;

    @Column(name = "ID_PseudoBar", nullable = false)
    private String pseudoBar;

    @Column(name = "balance", nullable = false)
    private double balance;

    @Column(name = "ID_Code", nullable = false)
    private String codeId;
    @Column(name = "ID_Root")
    private Integer rootId;

    public Integer getRootId() {
        return rootId;
    }

    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    public UserCardEntity() {
    }

    public String getPseudoBar() {
        return pseudoBar;
    }

    public void setPseudoBar(String pseudoBar) {
        this.pseudoBar = pseudoBar;
    }

    public UserCardEntity(String targetId) {
        this.targetId = targetId;
    }

    public UserCardEntity(String targetId, double balance) {
        this.targetId = targetId;
        this.balance = balance;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }


    public String display() {
        return this.getTargetId() + ":" + this.getBalance();
    }
}
