package model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "station")
public class StationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

	@Column(name = "name")
    private String name;
	
	@Column(name = "id_zone")
	private int idZone;

    public int getIdZone() {
		return idZone;
	}

	public void setIdZone(int idZone) {
		this.idZone = idZone;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.station1", cascade = CascadeType.ALL)
    private Set<LocationEntity> location1 = new HashSet<LocationEntity>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.station2", cascade = CascadeType.ALL)
    private Set<LocationEntity> location2 = new HashSet<LocationEntity>();

    public StationEntity() {
    }

    public Set<LocationEntity> getLocation1() {
        return location1;
    }

    public void setLocation1(Set<LocationEntity> location1) {
        this.location1 = location1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
