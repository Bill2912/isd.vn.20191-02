package model;

import javax.persistence.Basic;

//import net.bytebuddy.asm.Advice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.time.LocalDateTime;

@Entity
@Table(name = "24hticket")

public class TicketTFEntity {
    public Integer getRootId() {
        return rootId;
    }

    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    @Id
    @Column(name = "ID_Target")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "ID_PseudoBar", nullable = false)
    private String pseudoBar;

    @Column(name = "ID_Root")
    private Integer rootId;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "active_time")
    public LocalDateTime active_time;


    public TicketTFEntity() {
		super();
	}

	public TicketTFEntity(String id, LocalDateTime active_time) {
        this.id = id;
        this.active_time = active_time;
    }

    public TicketTFEntity(String id) {
        this.id = id;
    }

    public TicketTFEntity(String id, LocalDateTime active_time, int rootId) {
        this.id = id;
        this.active_time = active_time;
        this.rootId = rootId;
    }

    public String getPseudoBar() {
        return pseudoBar;
    }

    public void setPseudoBar(String pseudoBar) {
        this.pseudoBar = pseudoBar;
    }

    public String display(int i) {
        LocalDateTime datenow = LocalDateTime.now();
        if (i == 1) {
            if (this.getActive_time() != null) {
                return "Type: 24h ticket    ID: " + this.getId() + "\nValid until " + this.getActive_time().plusDays(1);
            }
            else{
                return "Type: 24h ticket    ID: " + this.getId() + "\nValid until " + datenow.plusDays(1) ;
            }
        }
        else{
            if (this.getActive_time() != null){
                return "Type: 24h ticket    ID: " + this.getId() + "\nValid until " + this.getActive_time().plusDays(1);
            }
            else{
                return "Type: 24h ticket    ID: " + this.getId() + "\nValid until null" ;
            }
        }
    }

    public LocalDateTime getActive_time() {
        return active_time;
    }

    public void setActive_time(LocalDateTime active_time) {
        this.active_time = active_time;
    }


}