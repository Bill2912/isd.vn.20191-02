package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "target")
public class TargetEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_Ticket", nullable = false, unique = true, length = 16)
    private String idTicket;

    @Column(name = "ID_Code", nullable = false, unique = true, length = 16)
    private String idCode;

    public TargetEntity(String idTicket, String idCode) {
        this.idTicket = idTicket;
        this.idCode = idCode;
    }

    public TargetEntity() {
    }

    public String getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(String idTicket) {
        this.idTicket = idTicket;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }
}
