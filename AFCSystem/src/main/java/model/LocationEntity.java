package model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "location")
@AssociationOverrides({
	@AssociationOverride(name = "pk.station1", joinColumns = @JoinColumn(name = "ID_Station1")),
	@AssociationOverride(name = "pk.station2", joinColumns = @JoinColumn(name = "ID_Station2"))
})
public class LocationEntity implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @EmbeddedId
    private LocationId pk = new LocationId();

//    @Id
//    @Column(name = "ID_Station1")
//    private int station1;
//
//    @Id
//    @Column(name = "ID_Station2")
//    private int station2;

    @Column(name = "distance")
    private Double distance;

    
    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public StationEntity getStation1() {
    	return this.pk.getStation1();
    }

    public StationEntity getStation2() {
    	return this.pk.getStation2();
    }
    
    public LocationEntity() {
		
    }

    public LocationEntity(StationEntity station1, StationEntity station2, Double distance) {
        this.pk.setStation1(station1);
        this.pk.setStation2(station2);
        this.distance = distance;
	}

}
