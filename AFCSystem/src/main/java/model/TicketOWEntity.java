package model;

import javax.persistence.*;

@Entity
@Table(name = "onewayticket")
public class TicketOWEntity {
    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private static final long serialVersionUID = 7527884744074835768L;
    @Id
    @Column(name = "ID_Target")
    private String id;

    @Column(name = "status")
    private int status;

    @Column(name = "ID_Disembarkation")
    private Integer disembarkation;

    @Column(name = "ID_Embarkation")
    private Integer embarkation;

    @Column(name = "ID_PseudoBar", nullable = false)
    private String pseudoBar;

    @Column(name = "ID_Real_Start_Station")
    private Integer real_station_start;

    @Column(name = "cost")
    private double cost;
    
    @Column(name = "ID_line")
    private int idLine;

    public int getIdLine() {
		return idLine;
	}

	public void setIdLine(int idLine) {
		this.idLine = idLine;
	}

	public TicketOWEntity(String id, int status, Integer disembarkation, Integer embarkation, Integer real_station, double cost) {
        super();
        this.id = id;
        this.status = status;
        this.disembarkation = disembarkation;
        this.embarkation = embarkation;
        this.real_station_start = real_station;
        this.cost = cost;
    }

    public TicketOWEntity(String id, Integer disembarkation, Integer embarkation, double cost) {
        super();
        this.id = id;
        this.disembarkation = disembarkation;
        this.embarkation = embarkation;
        this.cost = cost;
    }

    public TicketOWEntity(int status, Integer disembarkation, Integer embarkation, Integer real_station_start, double cost) {
        super();
        this.status = status;
        this.disembarkation = disembarkation;
        this.embarkation = embarkation;
        this.real_station_start = real_station_start;
        this.cost = cost;
    }

    public TicketOWEntity(String id) {
        super();
        this.id = id;
        // TODO Auto-generated constructor stub
    }
	public String display() {
	    return "Type : One Way Ticket" + "\t" + this.getId() +"\n" + "Balance :" + this.getCost();
	}
    public TicketOWEntity() {

    }

    public String getPseudoBar() {
        return pseudoBar;
    }

    public void setPseudoBar(String pseudoBar) {
        this.pseudoBar = pseudoBar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getDisembarkation() {
        return disembarkation;
    }

    public Integer getEmbarkation() {
        return embarkation;
    }

    public void setEmbarkation(Integer embarkation) {
        this.embarkation = embarkation;
    }

    public Integer getReal_station_start() {
        return real_station_start;
    }

    public void setReal_station_start(Integer real_station_start) {
        this.real_station_start = real_station_start;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setDisembarkation(Integer disembarkation) {
        this.disembarkation = disembarkation;
    }
    
    


}
