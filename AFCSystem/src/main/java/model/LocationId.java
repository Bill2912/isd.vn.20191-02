package model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class LocationId implements Serializable{
	@ManyToOne
	private StationEntity station1;
	
	@ManyToOne
	private StationEntity station2;

	public StationEntity getStation1() {
		return station1;
	}

	public void setStation1(StationEntity station1) {
		this.station1 = station1;
	}

	public StationEntity getStation2() {
		return station2;
	}

	public void setStation2(StationEntity station2) {
		this.station2 = station2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((station1 == null) ? 0 : station1.hashCode());
		result = prime * result + ((station2 == null) ? 0 : station2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationId other = (LocationId) obj;
		if (station1 == null) {
			if (other.station1 != null)
				return false;
		} else if (!station1.equals(other.station1))
			return false;
		if (station2 == null) {
			if (other.station2 != null)
				return false;
		} else if (!station2.equals(other.station2))
			return false;
		return true;
	}

//	public LocationId(StationEntity station1, StationEntity station2) {
//		super();
//		this.station1 = station1;
//		this.station2 = station2;
//	}
	
	
}
