
import java.time.LocalDateTime;
import java.util.List;
import model.LineEntity;
import model.TicketTFEntity;
import service.LineService;
import service.LineServiceImp;
import service.TicketTFService;
import service.TicketTFServiceImp;
public class TestQueryLocation {
    public static void main(String[] args) {
        TicketTFService service = new TicketTFServiceImp();


        TicketTFEntity userCardEntity = service.getById("TF201911260002");
        System.out.println(userCardEntity.getActive_time());

//		PC201911260003
//		TF201911260002

        LocalDateTime time = LocalDateTime.parse("2017-11-15T08:22:12");

        service.updateTime("TF201911260002", time);

        TicketTFEntity tick = service.getById("TF201911260002");
        System.out.println(tick.getActive_time());

//		LocalDateTime time2 = LocalDateTime.parse("1998-05-19T12:30:12");
//
//		service.updateTime("TF201911260002", time2);
//
//		TicketTFEntity tick2 = service.getById("TF201911260002");
//		System.out.println(tick2.getActive_time());

        LineService service2 = new LineServiceImp();

        LineEntity line = service2.getById(1);

        System.out.println(line.getRoutes());

    }
}
