package service;

import java.time.LocalDateTime;
import java.util.List;

import model.TicketTFEntity;

public interface TicketTFService {
    TicketTFEntity getById(String id);
    
    List<TicketTFEntity> getAll();

    void updateRootId(String ticketid,Integer rootId);
    
    void updateTime(String ticketid, LocalDateTime active_time);
}
