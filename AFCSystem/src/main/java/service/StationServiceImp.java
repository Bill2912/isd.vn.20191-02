package service;

import model.LocationEntity;
import model.StationEntity;
import repository.LocationRepository;
import repository.StationRepository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class StationServiceImp implements StationService {
	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	
    private StationRepository stationRepository= new StationRepository();
	
    @Override
    public StationEntity getByName(String name) {
		Session ses = fac.openSession();
    	StationEntity sta = this.stationRepository.getByName(name, ses);
		ses.getTransaction().commit();
    	return sta;
    }
	@Override
	public StationEntity getById(int id) {
		Session ses = fac.openSession();
		StationEntity re = this.stationRepository.getById(id, ses);
		ses.getTransaction().commit();
		return re;
	}
    @Override
	public List<StationEntity> getAll() {
		Session ses = fac.openSession();
    	List<StationEntity> stas = this.stationRepository.getAll(ses);

		ses.getTransaction().commit();
    	return stas;
    }


}
