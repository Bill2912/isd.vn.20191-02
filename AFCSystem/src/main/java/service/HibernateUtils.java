package service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtils {
	private static final SessionFactory fac = buildSessionFactory();
	
	private static SessionFactory buildSessionFactory() {
		try	{
			ServiceRegistry reg = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			
			Metadata meta = new MetadataSources(reg).getMetadataBuilder().build();
			
			return meta.getSessionFactoryBuilder().build();
		} catch(Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return fac;
	}
	
	public static void factoryShutDown() {
		getSessionFactory().close();
	}
}
