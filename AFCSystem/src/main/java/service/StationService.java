package service;

import model.StationEntity;

import java.util.List;

public interface StationService {
    List<StationEntity> getAll();
    
    StationEntity getByName(String name);
    
    StationEntity getById(int id);
}
