package service;

import java.util.List;

import model.LineEntity;

public interface LineService {

	public List<LineEntity> getAll();
	
	public LineEntity getById(int id);
}
