package service;

import java.util.List;

import model.TicketOWEntity;

public interface TicketOWService {
	void updateStatus(String ticke_id,int status);
	
	void setRealStationStart(String ticke_id, int rootid);
	
	TicketOWEntity getTicketbyId(String id);
	
	List<TicketOWEntity> getAll();
}
