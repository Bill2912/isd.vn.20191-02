package service;

import java.util.List;

import model.UserCardEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import repository.UserCardRepository;

public class UserCardServiceImp implements UserCardService {
    private UserCardRepository userCardRepository = new UserCardRepository();

	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	
	@Override
	public UserCardEntity getById(String id) {
		Session ses = fac.openSession();
		UserCardEntity re= userCardRepository.getById(id,ses);
		ses.getTransaction().commit();
		return re;
	}

	@Override
	public List<UserCardEntity> getAll() {
		Session ses = fac.openSession();
		List<UserCardEntity> re = userCardRepository.getAll(ses);
		ses.getTransaction().commit();
		return re;
	}

	@Override
	public void updateRootId(String targetId, Integer rootId) {
		Session ses = fac.openSession();
		userCardRepository.setRootId(targetId, rootId, ses);
		ses.getTransaction().commit();
	}

    @Override
    public void updateBalance(String id,double balance) {
		Session ses = fac.openSession();
    	userCardRepository.setBalance(id, balance, ses);
    	ses.getTransaction().commit();
    }


}
