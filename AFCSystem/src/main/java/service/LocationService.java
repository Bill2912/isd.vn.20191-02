package service;

import java.util.List;

import model.LocationEntity;

public interface LocationService {
	LocationEntity getById(int station1, int station2);

	List<LocationEntity> getAllLocation();
}