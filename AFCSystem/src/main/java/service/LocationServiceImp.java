package service;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import model.LocationEntity;
import repository.LocationRepository;

public class LocationServiceImp implements LocationService {
    private LocationRepository locationRepository =new LocationRepository();
	
	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	private static Session ses = fac.getCurrentSession();
    
    @Override
    public LocationEntity getById(int station1, int station2) {
		Session ses = fac.openSession();
		LocationEntity re = this.locationRepository.getLocationbyID(station1, station2,ses);

		ses.getTransaction().commit();
		return re;
    }

    @Override
	public List<LocationEntity> getAllLocation() {
		Session ses = fac.openSession();
    	List<LocationEntity> locas = this.locationRepository.getLocation(ses);

		ses.getTransaction().commit();
    	return locas;
    }
    
}