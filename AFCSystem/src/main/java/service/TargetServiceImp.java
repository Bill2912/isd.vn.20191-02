package service;

import model.TargetEntity;
import repository.TargetRepository;
import repository.UserCardRepository;

import java.lang.annotation.Target;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TargetServiceImp implements TargetService {
    private TargetRepository targetRepository = new TargetRepository();
    
	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	
    @Override
    public TargetEntity getTargetbyId(String id) {
		Session ses = fac.openSession();
		TargetEntity re = targetRepository.getById(id, ses);
		ses.getTransaction().commit();
		return re;
    }

	@Override
	public List<TargetEntity> getAll() {
		Session ses = fac.openSession();
		List<TargetEntity> re = targetRepository.getAll(ses);
		ses.getTransaction().commit();
		return re;
	}


}
