package service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import model.LineEntity;
import repository.LineRepository;

public class LineServiceImp implements LineService{
	private LineRepository repo = new LineRepository();
	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	
	@Override
	public List<LineEntity> getAll() {
		Session ses = fac.openSession();
		List<LineEntity> re = repo.getAll(ses);
		ses.getTransaction().commit();
		ses.close();
		return re;
	}

	@Override
	public LineEntity getById(int id) {
		Session ses = fac.openSession();
		LineEntity re = repo.getById(id, ses);
		ses.getTransaction().commit();
		ses.close();
		return re;
	}
	
}
