package service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import model.TicketOWEntity;
import repository.OneWayTicketRepository;

public class TicketOWServiceImp implements TicketOWService {
	OneWayTicketRepository oneTicRepo = new OneWayTicketRepository();

	private static SessionFactory fac = HibernateUtils.getSessionFactory();

	@Override
	public void updateStatus(String ticke_id,int status) {
		Session ses = fac.openSession();
		oneTicRepo.setStatus(ticke_id, status, ses);
		ses.getTransaction().commit();
		ses.close();
	}
	
	@Override
	public void setRealStationStart(String ticke_id, int rootid) {
		Session ses = fac.openSession();
		oneTicRepo.setRootId(ticke_id, rootid, ses);
		ses.getTransaction().commit();
		ses.close();
	}

	@Override
	public TicketOWEntity getTicketbyId(String id) {
		Session ses = fac.openSession();
		TicketOWEntity ticket = oneTicRepo.getById(id, ses);
		ses.getTransaction().commit();
		ses.close();
		return ticket;
	}

	@Override
	public List<TicketOWEntity> getAll() {
		Session ses = fac.openSession();
		List<TicketOWEntity> ticketList = oneTicRepo.getAll(ses);
		ses.getTransaction().commit();
		ses.close();
		return ticketList;
	}
}
