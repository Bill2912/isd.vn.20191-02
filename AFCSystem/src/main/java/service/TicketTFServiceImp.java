package service;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import model.TicketTFEntity;
import repository.TicketTFRepository;

public class TicketTFServiceImp implements TicketTFService {
	TicketTFRepository tickRepo = new TicketTFRepository();

	private static SessionFactory fac = HibernateUtils.getSessionFactory();
	
	@Override
	public TicketTFEntity getById(String id) {
		Session ses = fac.openSession();
		TicketTFEntity re = tickRepo.getById(id,ses);
		ses.getTransaction().commit();
		return re;
	}

	@Override
	public List<TicketTFEntity> getAll() {
		Session ses = fac.openSession();
		List<TicketTFEntity> re = tickRepo.getAll(ses);
		ses.getTransaction().commit();
		return re;
	}

	@Override
	public void updateRootId(String ticketid,Integer rootId) {
		Session ses = fac.openSession();
		tickRepo.setRootId(ticketid, rootId, ses);
		ses.getTransaction().commit();
	}

    @Override
    public void updateTime(String ticketid, LocalDateTime active_time) {
    	Session ses = fac.openSession();
    	tickRepo.updateTime(ticketid, active_time, ses);
    	ses.getTransaction().commit();    	
    }


}
