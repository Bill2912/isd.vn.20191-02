package service;

import model.UserCardEntity;

import java.util.List;

public interface UserCardService {
    UserCardEntity getById(String id);
    
    List<UserCardEntity> getAll();
    
    void updateRootId(String targetId,Integer rootid);
    
    void updateBalance(String targetId,double balance);
}
