package service;

import java.util.List;

import model.TargetEntity;

public interface TargetService {
	
	List<TargetEntity> getAll();
	
    TargetEntity getTargetbyId(String id);
}
