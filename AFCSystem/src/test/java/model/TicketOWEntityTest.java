package model;

import org.junit.Assert;
import org.junit.Test;

import service.TicketOWService;
import service.TicketOWServiceImp;
public class TicketOWEntityTest {
	TicketOWService ticketOWService = new TicketOWServiceImp();
	TicketOWEntity oneWayTicket =  ticketOWService.getTicketbyId("OW201911260002");
    @Test
	public void checkPseudoBar(){
    	Assert.assertEquals("minhdana",oneWayTicket.getPseudoBar());
	}
	@Test
	public void checkStatus(){
		Assert.assertEquals(0,oneWayTicket.getStatus());
	}
	@Test
	public void checkID_Disembarkation(){
		Assert.assertEquals(0,oneWayTicket.getDisembarkation()-2);
	}
	@Test
	public void checkID_Embarkation(){
		Assert.assertEquals(0,oneWayTicket.getEmbarkation()-6);
	}
	@Test
	public void checkReal_Start_Station(){
    	Assert.assertEquals(0,oneWayTicket.getReal_station_start()-3);
	}
	@Test
	public void checkCost(){
    	Assert.assertEquals(3.9,oneWayTicket.getCost(),0.001);
	}

}
