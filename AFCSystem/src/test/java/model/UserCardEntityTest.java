package model;

import org.junit.Assert;
import org.junit.Test;
import repository.UserCardRepository;
import service.UserCardService;
import service.UserCardServiceImp;

public class UserCardEntityTest {
    /**
     * This method to test initial object
     */
    @Test
    public void test_user_card_entity() {
        UserCardEntity userCard = new UserCardEntity("PC201911131234", 10.0);

        Assert.assertEquals("PC201911131234:10.0", userCard.display());
    }

}
