package model;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class TicketTFEntityTest {
    /**
     * This method to test initial object
     */
    @Test
    public void test_ticket24h_entity() {
        TicketTFEntity ticket24h = new TicketTFEntity("PC201911131234", LocalDateTime.of(2019,11,23,14,5,0),2);
        Assert.assertEquals("Type: 24h ticket    ID: PC201911131234\nValid until 2019-11-24T14:05",ticket24h.display(1));
        System.out.print(ticket24h.display(1));
    }
}