package controller;

import model.UserCardEntity;
import org.junit.Assert;
import org.junit.Test;
import service.UserCardService;
import service.UserCardServiceImp;

import java.util.ArrayList;
import java.util.List;

public class CardControllerTest {
    CardController cardController = new CardController();
    UserCardService userCardService = new UserCardServiceImp();

    /**
     * This method to test calculate amount with distance
     */
    @Test
    public void test_amount() {
        Assert.assertNotEquals("so tien la:2.3", cardController.displayAmount(1));
        Assert.assertEquals("so tien la:2.3", cardController.displayAmount(6.3));
        Assert.assertEquals("so tien la:3.1", cardController.displayAmount(10.3));
        Assert.assertEquals("so tien la:3.5", cardController.displayAmount(12.3));

    }

    /**
     * This method to test request validation enter
     */
    @Test
    public void test_condition_enter() {
        UserCardEntity userCard = userCardService.getById("PC201911260003");
        String condition = cardController.requestValidationEnter(userCard, 2);
        System.out.println(condition);
    }
    /**
     * This method to test request validation exit
     */
    @Test
    public void test_condition_Exit() {
        UserCardEntity userCard = userCardService.getById("PC201911260003");
        String condition = cardController.requestValidationExit(userCard, 6);
        System.out.println(condition);
    }


}
