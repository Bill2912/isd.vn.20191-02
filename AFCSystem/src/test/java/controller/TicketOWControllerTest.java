package controller;

import org.junit.Assert;
import org.junit.Test;
import model.TicketOWEntity;
import util.*;
import util.CalculateAmountby2km;
import util.GetDistance;

import static org.junit.Assert.assertEquals;
import static util.Constant.REQUEST_CLOSE_GATE;
import static util.Constant.REQUEST_OPEN_GATE;

public class TicketOWControllerTest {

	TicketOWController test = new TicketOWController();
	TicketOWEntity ticket1 = new TicketOWEntity();

	@Test
	public void test_checkStatus() {
		ticket1.setStatus(0);
		assertEquals(true, test.CheckStatus(ticket1));
		ticket1.setStatus(1);
		assertEquals(false, test.CheckStatus(ticket1));
	}
	@Test
	public void test_checkBalance() {
		ticket1.setCost(2.7);
		ticket1.setReal_station_start(1);
		//CalculateAmountby2km calculateAmountby2km = new CalculateAmountby2km();
		//GetDistance getDistance = new GetDistance();
		//System.out.println("tien that" +calculateAmountby2km.calculateAmount(getDistance.getDistanceByID(ticket1.getReal_station_start(),4)));
		//System.out.println("tien di " +ticket1.getCost());
		Assert.assertEquals(false, test.CheckBalance(ticket1, 4));
		Assert.assertEquals(true, test.CheckBalance(ticket1, 2));
	}
	@Test
	public void test_checkStation() {
		ticket1.setIdLine(1);
		ticket1.setDisembarkation(1);
		ticket1.setEmbarkation(4);
		Assert.assertEquals(true, test.CheckStation(ticket1, 4));
		Assert.assertEquals(false, test.CheckStation(ticket1, 6));
		Assert.assertEquals(true, test.CheckStation(ticket1, 4));
	}

	@Test
	public void test_ValidRequestEnter() {
		//true
		ticket1.setIdLine(1);
		ticket1.setDisembarkation(1);
		ticket1.setEmbarkation(4);
		ticket1.setStatus(0);
		String check_true= REQUEST_OPEN_GATE;
		String check_false= REQUEST_CLOSE_GATE;
		//System.out.println(test.requestValidationEnter(ticket1, 3));
		//Assert.assertEquals("","Opening Gate",test.ValidRequestEnter(ticket1, 3));
		Assert.assertEquals(true,check_true.equals(test.requestValidationEnter(ticket1,3)));

	   //assertEquals(java.util.Optional.of(3),ticket1.getReal_station_start());
	   //assertEquals(3,ticket1.getReal_station_start(),0.1);
		//false
		ticket1.setReal_station_start(0);
		//Assert.assertEquals("","Invalid One Way Ticket",test.ValidRequestEnter(ticket1, 6));
		Assert.assertEquals(true,check_false.equals(test.requestValidationEnter(ticket1,6)));
		//assertEquals(0,ticket1.getReal_station_start(),0);

	}
	@Test
	public void test_ValidRequestExit() {
		//true
		String check_true= REQUEST_OPEN_GATE;
		//false
		String check_false= REQUEST_CLOSE_GATE;
		ticket1.setDisembarkation(1);
		ticket1.setEmbarkation(4);
		ticket1.setReal_station_start(1);
		ticket1.setStatus(0);
		ticket1.setCost(2.7);
		ticket1.setIdLine(1);
		System.out.println(test.requestValidationExit(ticket1,4));
		//Assert.assertEquals(true,check_false.equals(test.ValidRequestExit(ticket1,4)));
		Assert.assertEquals(true,check_true.equals(test.requestValidationExit(ticket1,2)));

	}

//assertSame(string3, string4);
//"assertSame()" functionality is to check that the two objects refer to the same object.
//
//Since string3="test" and string4="test" means both string3 and string4 are of the same type so assertSame(string3, string4) will return true.

}
