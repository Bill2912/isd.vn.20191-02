package controller;

import model.TicketTFEntity;
import org.junit.Test;
import org.junit.Assert;
import service.TicketTFService;
import service.TicketTFServiceImp;

import java.time.LocalDateTime;

public class TicketTFControllerTest {
    private LocalDateTime datetime = LocalDateTime.now();
    private TicketTFService ticket24hservice = new TicketTFServiceImp();

    private TicketTFController test1 = new TicketTFController(ticket24hservice);
    private TicketTFEntity ticket2 = ticket24hservice.getById("TF201911260002"); // root != null
    private TicketTFEntity ticket4 = ticket24hservice.getById("TF201912030002"); // less24h + root == null
    private TicketTFEntity ticket1 = ticket24hservice.getById("TF201911260052"); // over24h + root == null
    private TicketTFEntity ticket3 = ticket24hservice.getById("TF201912030003"); // null + no time

    private TicketTFEntity ticket5 = ticket24hservice.getById("TF201911260051"); // null + no time

    /**
     * This method to test request validation enter
     */
    @Test
    public void test_condition_enter(){
        Assert.assertEquals("Open Gate", test1.requestValidationEnter(ticket4,  1));
        Assert.assertEquals("Close Gate", test1.requestValidationEnter(ticket2,  1));
        Assert.assertEquals("Close Gate", test1.requestValidationEnter(ticket1,  1));
        Assert.assertEquals("Open Gate", test1.requestValidationEnter(ticket3,  1));

        ticket24hservice.updateRootId(ticket4.getId(), null);
        ticket24hservice.updateRootId(ticket3.getId(), null);
        ticket24hservice.updateTime(ticket3.getId(), null);
    }

    /**
     * This method to test request validation exit
     */
    @Test
    public void test_condition_exit() {
        int before_test_rootId = ticket2.getRootId();
        Assert.assertEquals("Close Gate", test1.requestValidationExit(ticket5, 1));
        Assert.assertEquals("Open Gate", test1.requestValidationExit(ticket2, 1));
        ticket24hservice.updateRootId(ticket2.getId(), before_test_rootId);
    }
}