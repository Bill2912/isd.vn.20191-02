*Subject:  Software Development  
Lecturer: Prof. Nguyen Thi Thu Trang  *
# Repository :ISD.VN.20191-02
## Home-Work1.5 Use-Case Diagram
**Part 1: Ticket vending machine**
*     Design the use case diagram using Astah
**Part 2: Automated fare collection (AFC) system**   
*     Design the use case diagram using Astah
*     Design flow of events for use cases. 
*     Each member of the group needs to design the flow of events for 1 -> 2 use cases.
##      Team 2:
*    Nguyễn Minh Dân (Leader)
*    Phạm Việt Cường
*    Đặng Ngọc Diệp
*    Phùng Thành Công 
##    Work Assignment:
1. Enter one-time ticket  - Dân 
1.  Exit one-time ticket - Cường
1.  Enter 24h ticket - Diệp
1.  Exit 24h ticket- Diệp
1.  Enter card - Công
1.  Exit card - Công
