*Subject:  Software Development  
Lecturer: Prof. Nguyen Thi Thu Trang  *
# Repository :ISD.VN.20191-02
## Home-Work1.5 Use-Case Diagram
*Part 1: Ticket vending machine*
*     Design the use case diagram using Astah
*Part 2: Automated fare collection (AFC) system*   
*     Design the use case diagram using Astah
*     Design flow of events for use cases. 
*     Each member of the group needs to design the flow of events for 1 -> 2 use cases.
##      Team 2:
*    Nguyễn Minh Dân (Leader)
*    Phạm Việt Cường
*    Đặng Ngọc Diệp
*    Phùng Thành Công 
##    Work Assignment:

1.  Enter by prepaid card - Công
1.  Exit by prepaid card - Công
